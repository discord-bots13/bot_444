import sys
import logging
import discord
import asyncio
import datetime

LOG_FILE_NAME = 'bot_444.log'
PWD = 'C:\\dev\\bot_444\\'

TOKEN_DISCORD = ''
GUILD_NAME = 'SPECTRE'
SOUND_PATH = PWD + '444_Nuits.mp3'
CHANNEL_NAME = 'tests_dev'

logging.basicConfig(filename=PWD + LOG_FILE_NAME, format='%(asctime)s %(message)s', level=logging.WARNING)

bot = discord.Client()

voice_client = None

# ----------------------------------------------------------------
# ----------------------GESTION DES CHANNELS----------------------
# ----------------------------------------------------------------

# Recupere tous les channels du server
# qui sont des channels VOCAUX et qui on AU MOINS UNE personne de co dessus
def get_channels():
    guild = [x for x in bot.guilds if x.name == GUILD_NAME][0]
    channels = [c for c in guild.channels if 'voice' in c.type and len(c.voice_states) > 0]
    return channels


# Recupere le channel vocal avec le plus de gens a l'interieur
def get_biggest_channel():
    channels = get_channels()
    channel = None
    if len(channels) > 0:
        channel = channels[0]
        for c in channels[1:]:
            channel = c if len(c.voice_states) > len(channel.voice_states) else channel
    log_message = 'NOT FOUND' if channel is None else 'FOUND'
    logging.warning('[444] BIGGEST CHANNEL ' + log_message)
    return channel

# Verifie qu'un message est valide
def is_message_valid(message):
    return message.author != bot.user and message.channel.name == CHANNEL_NAME

# ----------------------------------------------------------------
# -------------------------GESTION DU SON-------------------------
# ----------------------------------------------------------------

#Verification que l'on est bien a la bonne heure
def check_hour():
   now = datetime.datetime.now()
   if now.hour == 4 and now.minute == 44:
        logging.warning('[444] HOUR  IS GOOD')
        return True
   else:
        logging.warning('[444] NOT GOOD HOUR')
        logging.shutdown()
        return True

# Creer un voice_client
async def start_voice_client(channel):
    channel = await bot.fetch_channel(channel.id)
    return await channel.connect(timeout=1, reconnect=False)


# Tout le processs d'envoi de son
async def play_vocal(channel):
    global voice_client
    voice_client = await start_voice_client(channel)
    logging.warning('[444] VOICE CLIENT STARTED')
    source = discord.FFmpegPCMAudio(SOUND_PATH)
    voice_client.play(source)
    logging.warning('[444] VOICE CLIENT IS PLAYING')

# deconnect le bot
async def disconnect():
    global voice_client
    try:
        voice_client.stop()
        await voice_client.disconnect()
    except TypeError as e:
        logging.shutdown()
        print(e)
        pass
    voice_client = None
    logging.shutdown()
    sys.exit()

# ----------------------------------------------------------
# ----------------------------------------------------------
# FONCTION EXECUTE AU LANDEMENT DU BOT (LANCEMENT DU SCRIPT)
# ----------------------------------------------------------
# ----------------------------------------------------------

# Au lancement du bot
@bot.event
async def on_ready():
    if check_hour():
        logging.warning('[444] BOT STARTED')
        # Trouver le channel vocal avec le plus de personnes dessus
        channel = get_biggest_channel()
        # Jouer le son
        if channel is not None:
            await play_vocal(channel)
            await asyncio.sleep(179)
            await disconnect()

    logging.shutdown()
    sys.exit()


# permet de disconnect le bot
@bot.event
async def on_message(message):
    if is_message_valid(message) and message.content.startswith('$deco'):
        await disconnect()

def main():
    logging.warning('[444] SCRIPT STARTED')
    bot.run(TOKEN_DISCORD)

if __name__ == '__main__':
   main()
